package mision.tic.ArchivosO59.controlador;

import mision.tic.ArchivosO59.servicio.ServicioArchivos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api")
public class ControladorArchivos {

    @Autowired
    ServicioArchivos servicioArchivos;

    @PostMapping("/guardarArchivo")
    public void guardarArchivo(@RequestBody MultipartFile archivo){
        servicioArchivos.guardarArchivo(archivo);
    }

    @GetMapping("/obtenerArchivo")
    public void obtenerArchivo(HttpServletResponse response,@RequestParam String nombre,@RequestParam String mimetype){
        servicioArchivos.obtenerArchivo(response,nombre,mimetype);

    }

}
