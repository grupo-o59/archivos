package mision.tic.ArchivosO59.servicio;

import mision.tic.ArchivosO59.modelo.Archivo;
import mision.tic.ArchivosO59.repositorio.RepositorioArchivos;
import mision.tic.ArchivosO59.util.Config;
import org.apache.tika.mime.MimeType;
import org.apache.tika.mime.MimeTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class ServicioArchivos {
    @Autowired
    RepositorioArchivos repositorioArchivos;


    public void guardarArchivo(MultipartFile archivo) {
        if(!archivo.isEmpty()){
            Path directorio = Paths.get(Config.directorioArchivos);
            try{
                //Todo A futuro directorio si no existe el lo cree

                byte[] byteArchivo = archivo.getBytes();
                String rutaAbsoluta = directorio.toFile().getAbsolutePath();
                Path rutaArchivo = Paths.get(rutaAbsoluta+"//"+archivo.getOriginalFilename());
                Files.write(rutaArchivo,byteArchivo);
                String mimetype = archivo.getContentType();
                String nombre = archivo.getOriginalFilename().split("\\.")[0]; //Todo a futuro solucionar
                String urlImagen = "http://localhost:8082/api/obtenerArchivo?nombre="+nombre+"&mimetype="+mimetype;
                Archivo file = new Archivo(nombre,urlImagen);
                repositorioArchivos.save(file);

            }catch (Exception e){
                System.err.println(e.toString());
            }
        }
    }

    public void obtenerArchivo(HttpServletResponse response, String nombre, String mimetype) {
        try{
            MimeTypes allMime = MimeTypes.getDefaultMimeTypes();
            MimeType mime = allMime.forName(mimetype);
            String extension = mime.getExtension();
            response.setContentType(mimetype+"charset=utf-8");
            response.setHeader("Content-Disposition","inline; filename=archivo"+extension);
            ServletOutputStream salida = response.getOutputStream();
            salida.write(Files.readAllBytes(Paths.get(Config.directorioArchivos).resolve(nombre+extension)));
            salida.flush();
            salida.close();


        }catch (Exception e){
            System.err.println(e);
        }

    }
}
