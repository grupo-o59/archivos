package mision.tic.ArchivosO59.repositorio;

import mision.tic.ArchivosO59.modelo.Archivo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioArchivos extends JpaRepository<Archivo,Integer> {
}
