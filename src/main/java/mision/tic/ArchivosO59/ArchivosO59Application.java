package mision.tic.ArchivosO59;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArchivosO59Application {

	public static void main(String[] args) {
		SpringApplication.run(ArchivosO59Application.class, args);
	}

}
